/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userservice;

/**
 *
 * @author dvandeveerdonk
 */
public class User {
    int studentNr;
    String firstName;
    String lastName;

    @Override
    public String toString() {
        return "User{" + "studentNr=" + studentNr + ", firstName=" + firstName + ", lastName=" + lastName + '}';
    }
}
